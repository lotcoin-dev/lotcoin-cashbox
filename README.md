Welcome to _Lotcoin Cashbox_, the Lotcoin offline cashbox app for your Android device!

This project contains several sub-projects:

 * __cashbox__:
     The Android app itself. This is probably what you're searching for.
 * __market__:
     App description and promo material for the Google Play app store.

You can build all sub-projects at once using Maven:

`mvn clean install`
